import argparse
from exp.nb_acer import *

parser = argparse.ArgumentParser()
parser.add_argument('--name', type=str, required=False)

if __name__ == '__main__':
    args = parser.parse_args()
    
    log_vars = [
        'reward',
        'policy_loss',
        'value_loss',
        'temporal_diffs',
    ]

    print_vars = [
        'step',
        'reward',
        'policy_loss',
        'value_loss',
        'temporal_diffs',
        'time'
    ]

    print_vars = []
    
    reset_vars = [
        'reward',
        'policy_loss',
        'value_loss',
        'temporal_diffs',
    ]

    logger_events = [
        StepEvent(),
        RewardEvent(),
        PolicyLossEvent(),
        ValueLossEvent(), 
        TemporalDiffsEvent(), 
        TimeEvent(),
        DoneEvent(log_vars, print_vars, reset_vars),
    ]

    #env_name = 'Pendulum-v0'
    #env_name = 'Walker2DBulletEnv-v0'
    #env_name = 'HopperBulletEnv-v0'
    #env_name = 'AntBulletEnv-v0'
    env_name = 'HalfCheetahBulletEnv-v0'
    #env_name = 'HumanoidBulletEnv-v0'
    #env_name = 'LunarLanderContinuous-v2'
    #env_name = 'BipedalWalker-v3'
    #env_name = 'BipedalWalkerHardcore-v3'
    
    params = {
        'env_name': env_name,
        'policy_std': 0.3,
        'scale_mu_layer': False,
        'scale_val_layer': False,
        'policy_lr': 1e-05,
        'value_lr': 1e-05,
        'buffer_size': 1000000,
        'total_steps': 3000000,
        'min_replay_to_train': 1000,
        'num_steps': 1,
        'training_steps': 1,
        'batch_size': 256,
        'gamma': 0.99,
        'lmbd': 0.9,
        'min_k': 1,
        'max_k': 40
        'same_length_buffer': False
    }
    
    experiment(params, logger_events, experiment_name=args.name)
