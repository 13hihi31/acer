FROM pytorch/pytorch:1.8.0-cuda11.1-cudnn8-runtime

RUN apt-get update
RUN apt-get -y install python3-pip
RUN pip install gym
RUN pip install pybullet
