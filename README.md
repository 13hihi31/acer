# ACER - Actor Critic with Experience Replay
Implementation of ACER algorithm in PyTorch. Apparently there are two algorithms under the name ACER. This code base implements the one described in (Wawrzyński, 2009). The code can be run in a Jupyter notebook, from terminal or in a Docker container. Hyperparameters may need additional tuning to reach the top performance for this algorithm. 

# To Do
Train more policies, test the implementation further.

# References
* Paweł Wawrzyński. (2009) Real-time reinforcement learning by sequential actor–critics and experience replay. Neural Networks 22 (10), 1484–1497

# Results
Below are two plots showing the accumulated reward in each episode as the training progresses. The upper plot is from the version of the algorithm that samples trajectories of different lengths per batch as in (Wawrzyński, 2009). The lower plot comes from the modified algorithm that uses one random length for all trajectories per batch. The original algorithm is more stable during training but takes longer to execute than the modified algorithm with fixed trajectory length per batch.

![](plots/Cheetah_variable.jpg "Total episode reward as training progresses for varaible trajectory length per batch")
![](plots/Cheetah_fixed.jpg "Total episode reward as training progresses for fixed trajectory length per batch")
